// Revisar los campos obligatorios
function revisar(elemento)
{
    if(elemento.value=='')
    {
        alertify.set('notifier','position', 'top-center');
        alertify.error('ERROR THE "'+ elemento.name + '" FIELD IS REQUIRED', 5);
    }
}
// Crear un nuevo reporte via ajax
function newUser()
{
    var email = $('#email').val();
    var status_id = $('#status_id').val();
    var password = $('#password').val();
    var password_confirm = $('#password-confirm').val();

    var token = $('#token').val();
    var route = '/User';

    if (email == '' || status_id == '' || password == '' || password_confirm == '')
    {
        alertify.set('notifier','position', 'top-center');
        alertify.error('THE MANDATORY FIELDS, (*) WERE NOT REGISTERED', 5);  // alert will have class 'alert-color'
    }
    else
    {
        if (password == password_confirm)
        {
            $.ajax(
            {
                url: route,
                headers:
                {
                    'X-CSRF-TOKEN': token
                },
                type: 'POST',
                dataType: 'json',
                data:
                {
                    email: email,
                    status_id: status_id,
                    password: password,
                },
                success:function()
                {
                    alertify.set('notifier','position', 'top-center');
                    alertify.success('THE CHANGES HAVE BEEN SAVED SUCCESSFULLY', 8);
                    location.reload();
                },
                error: function (data)
                {
                    alertify.set('notifier','position', 'top-center');
                    alertify.error('PLEASE REVIEW THE INSERTED INFORMATION', 5);
                    alertify.error('SOMETHING WAS WRONG ...', 5);
                    console.log('Error:', data);
                }
            });
        }
        else
        {
            alertify.set('notifier', 'position', 'top-center');
            alertify.error('PASSWORDS DO NOT MATCH', 5);
        }
    }

}