function Mostrar(btn)
{
	var route = '/User/'+btn.value+'/edit';

	$.get(route, function(res)
	{
		$('#email_edit').val(res.email);
		$('#status_id_edit').val(res.status_id);
		$('#id').val(res.id);
	});
}

function editUser()
{
	var id = $('#id').val();
    var email = $('#email_edit').val();
    var status_id = $('#status_id_edit').val();
    var password = $('#password_edit').val();
    var password_confirm = $('#password-confirm_edit').val();

	var route = '/User/'+id+'';
	var token = $('#token').val();

	if (password == password_confirm)
	{
		$.ajax(
		{
			url: route,
			headers:
			{
				'X-CSRF-TOKEN': token
			},
			type: 'PUT',
			dataType: 'json',
			data:
			{
				email: email,
				status_id: status_id,
				password: password,
			},
			success:function()
			{
				alertify.set('notifier','position', 'top-center');
				alertify.success('THE CHANGES HAVE BEEN SAVED SUCCESSFULLY', 8);
				location.reload();
			},
			error: function (data)
			{
				alertify.set('notifier','position', 'top-center');
				alertify.error('PLEASE REVIEW THE INSERTED INFORMATION', 5);
				alertify.error('SOMETHING WAS WRONG ...', 5);
				console.log('Error:', data);
		    }
		});
	}
	else
	{
		alertify.set('notifier', 'position', 'top-center');
		alertify.error('PASSWORDS DO NOT MATCH', 5);
	}
};