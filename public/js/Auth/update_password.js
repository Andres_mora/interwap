function MostrarUser(btn)
{
	var route = '/User/'+btn.value+'/edit';

	$.get(route, function(res)
	{
		$('#name_edit_pas').val(res.name);
		$('#correo_edit_pas').val(res.email);
		$('#id').val(res.id);
	});
}

$('#editUserPassword').click(function()
{
	var id = $('#id').val();
	
	var name = $('#name_edit_pas').val();
	var email = $('#correo_edit_pas').val();
	var password = $('#password_edit_pas').val();
	var password_confirm = $('#password-confirm_edit_pas').val();

	var route = '/User/'+id+'';
	var token = $('#token').val();

	if (password == password_confirm)
	{
		$.ajax(
		{
			url: route,
			headers:
			{
				'X-CSRF-TOKEN': token
			},
			type: 'PUT',
			dataType: 'json',
			data:
			{			
				name: name,
				email: email,
				password: password,
			},
			success:function()
			{
				alertify.set('notifier','position', 'top-center');
				alertify.success('LOS CAMBIOS HAN SIDO GUARDADOS EXITOSAMENTE', 8);
				location.reload();
			},
			error: function (data)
			{
				alertify.set('notifier','position', 'top-center');
				alertify.error('POR FAVOR REVISA LA INFORMACION INSERTADA', 5);
				alertify.error('ALGO HA SALIDO MAL ...', 5);
		        console.log('Error:', data);
		    }
		});
	}
	else
	{
		alertify.set('notifier', 'position', 'top-center');
		alertify.error('LAS CONTRASEÑAS NO COINCIDEN', 5);
	}
});