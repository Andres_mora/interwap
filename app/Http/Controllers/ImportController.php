<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Modals\InformationUser;
use App\Modals\Status;
use App\Http\Controllers\Controller;
use Excel;

class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Import.list');
    }

    /**
     * Function Upload
     */
    public function upload(Request $request)
    {
        Excel::load($request->excel, function($reader)
    	{
            $i = 0;
		    $reader->each(function($sheet, $i)
		    {
                
                $result = $sheet->all();
                $i++;
                $user = new User;

                if($result['email'] != null)
                {
                    $user->email = $result['email'];
                }
                else
                {
                    echo '<br>'.'Falta el email en el rigistro: '.$i;
                }
                $user->password = bcrypt('123456789');


                $sutatus = Status::select('id')
                                        ->where('id', '=', $result['status'])
                                        ->first();

                if($sutatus != null)
                {
                    $user->status_id = $sutatus->id;
                }
                else
                {
                    echo '<br>'.'Falta el status en el rigistro: '.$i.'</br>'.'O el estado registrado no existe en la base de datos';
                }
                $user->save();
                
                $infU = new InformationUser;
                $infU->user_id = $user->id;
                
                if($result['name'] != null)
                {
                    $infU->name = $result['name'];
                }
                else
                {
                    echo '<br>'.'Falta el name en el rigistro: '.$i;
                }

                if($result['last_name'] != null)
                {
                    $infU->last_name = $result['last_name'];
                }
                else
                {
                    echo '<br>'.'Falta el last name en el rigistro: '.$i;
                }
                $infU->save();
            });
        });
        echo '<br>'.'<a href="/InformationUser">REVISAR INFORMACION GUARDADA</a>';
        dd('>>>');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
