<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Modals\Status;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = null;
        $statuses = null;
        
        $status = Status::orderBy('name')->get();

        $user = User::orderBy('email')
                        ->paginate(10);

        return view('auth.list')->with(compact('user', 'email', 'status', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ath.Modals.new');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $user = new User;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->status_id = $request->status_id;
            $user->save();

            return response()->json([
                'info' => 'save'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $email = $request->email;
        $statuses = $request->statuses;
    
        $status = Status::orderBy('name')->get();

        if($email != null)
        {
            $user = User::where('email', 'like', '%'.$email.'%')
                            ->orderBy('email')
                            ->paginate(10);
        }
        elseif ($statuses != null)
        {
            $user = User::where('status_id', '=', $statuses)
                            ->orderBy('email')
                            ->paginate(10);
        }
        else
        {
            $user = User::orderBy('email')
                            ->paginate(10);
        }

        return view('auth.list')->with(compact('user', 'email', 'status', 'statuses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return response()->json(
            $user->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax())
        {
            $user = User::find($id);
            $user->email = $request->email;
            if($request->password != null)
            {
                $user->password = bcrypt($request->password);
            }
            $user->status_id = $request->status_id;
            $user->save();
            
            return response()->json([
                'info' => 'save'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $User = User::find($id)
                    ->delete();
    }
}
