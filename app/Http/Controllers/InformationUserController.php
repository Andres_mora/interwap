<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modals\InformationUser;
use App\User;
use App\Modals\Status;

class InformationUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infU = InformationUser::whereHas('user', function($u)
                                    {
                                        $u->where('status_id', '=', '1');
                                    })
                                    ->orderBy('name')
                                    ->paginate(10);

                                    
        $infU_in = InformationUser::whereHas('user', function($u)
                                    {
                                        $u->where('status_id', '=', '2');
                                    })
                                    ->orderBy('name')
                                    ->paginate(10);

        $infU_es = InformationUser::whereHas('user', function($u)
                                    {
                                        $u->where('status_id', '=', '3');
                                    })
                                    ->orderBy('name')
                                    ->paginate(10);

        $user = User::orderBy('email')
                        ->get();
        
        $status = Status::orderBy('name')
                            ->get();

        return view('InformationUser.list')->with(compact('infU', 'user', 'status', 'infU_in', 'infU_es'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
