<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';
    protected $fillable = ['name'];
    protected $guarded = ['id'];
}
