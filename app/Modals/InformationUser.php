<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class InformationUser extends Model
{
    protected $table = 'information_users';
    protected $fillable = ['user_id', 'name', 'last_name'];
    protected $guarded = ['id'];

    /**
     * Consulta Belogns to
     */
    public function user()
    {
        return $this->belongsTo('App\USer', 'user_id', 'id');
    }
}
