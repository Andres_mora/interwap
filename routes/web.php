<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('/User', 'UserController');
Route::get('/User/Search', 'UserController@show');
Route::get('/User/{id}/delete', 'UserController@destroy');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/Import', 'ImportController');
Route::post('/Import/File', 'ImportController@upload');


Route::resource('/InformationUser', 'InformationUserController');
