<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

     <!-- Styles Bootstrap -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/Bootstrap/bootstrap-grid.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/Bootstrap/bootstrap-grid.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/Bootstrap/bootstrap-reboot.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/Bootstrap/bootstrap-reboot.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/Bootstrap/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/Bootstrap/bootstrap.min.css')}}">

    <!-- Style Alertyfy -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/alertify.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/alertify.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/alertify.rtl.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/alertify.rtl.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/themes/default.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/themes/default.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/themes/default.rtl.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/themes/default.rtl.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/themes/semantic.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/themes/semantic.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/themes/semantic.rtl.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/themes/semantic.rtl.min.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/themes/bootstrap.rtl.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/Alertyfy/themes/bootstrap.rtl.min.css') }}">

    <!-- Script Jquery -->
        <script src="{{ asset('/jquery/jquery.min.js')}}"></script>
        <script src="{{ asset('/jquery/jquery-2.1.4.js')}}"></script>        
        <script src="{{ asset('/jquery/moment.js')}}"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->email }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <!-- Script Alertyfy -->
        <script src="{{ asset('/js/Alertyfy/alertify.js')}}"></script>
        <script src="{{ asset('/js/Alertyfy/alertify.min.js')}}"></script>
</body>
</html>
