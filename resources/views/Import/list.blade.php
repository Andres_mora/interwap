@extends('layouts.app')

@section('content')
    <div class="container d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('INFORMATION UPLOAD FORM') }}</h1>
    </div>
    <div class="container">
        <form action="{{ url('/Import/File') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="jumbotron" style="background-color: #fff; border-style: groove; border-color: #E9E9E9; border-width:5px;">
                <div class="panel panel-header">
                    <label>{{ __('GENERATE IMPORT') }}</label>
                    <div class="input-group">
                        <label class="input-group-btn">
                            <span class="btn btn-outline-info">
                                <img src="{{ asset('feather/plus-square.svg') }}">
                                SELECT FILE&hellip; 
                                <input name="excel" type="file" style="display: none;" multiple required/>
                            </span>
                            <button type="submit" class="btn btn-outline-success">
                                <img src="{{ asset('feather/upload.svg') }}">
                                {{ __('SEND FORM') }}
                            </button>
                        </label>
                        <input type="text" class="form-control" readonly>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="{{ asset('js/Import/list.js') }}"></script>
@endsection