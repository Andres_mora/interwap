@extends('layouts.app')

@section('content')
    <div class="container d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('USER INFORMATION') }}</h1>
    </div>
    <div class="container">
        <form action="{{ url('#') }}" method="GET">
            {{ csrf_field() }}
            <div class="jumbotron" style="background-color: #fff; border-style: groove; border-color: #E9E9E9; border-width:5px;">
                <div class="panel panel-header">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label>{{ __('EMAIL')}}</label>
                            <input type="text" id="email_user" name="email" class="form-control" value="" style="text-transform:uppercase;"onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-md-6 -mb-3">
                          <label>{{ __('STATUS') }}</label>
                          <input type="hidden" id="status_2" value="">
                          <select id="status_id_list" class="form-control" name="statuses" onblur="revisar(this);">
                            <option value="" selected disabled>{{ __('SELECT A STATE') }}</option>
                            @foreach ($status as $statuses)
                                <option value="{{ $statuses->id }}">{{ $statuses->name }}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                </div>
                <div><br></div>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button type="button" class="btn btn-outline-primary" title="LIMPIAR FILTROS DE BUSQUEDA" onclick="Clear();">
                        <img src="{{ asset('feather/trash.svg') }}">
                    </button>
                    <button type="submit" class="btn btn-outline-success">
                        <img src="{{ asset('feather/search.svg') }}">
                        {{ __('SEARCH') }}
                    </button>
                </div>
            </div>
        </form>
            
        <div class="table-responsive">
            <h4>
                Usuarios Activos {{ $infU->total() }}
            </h4>
            <table class="table table-hover table-sm">
                <thead>
                    <tr>
                        <th><center>{{ __('EMAIL') }}</center></th>
                        <th><center>{{ __('NAME') }}</center></th>
                        <th><center>{{ __('LAST NAME') }}</center></th>
                    </tr>
                </thead>
                <tbody id="tableCustomer">
                    @foreach ($infU as $infUs)
                        <tr>
                            <td class="hoverable"><center>{{$infUs->user->email}}</center></td>
                            <td class="hoverable"><center>{{$infUs->name}}</center></td>
                            <td class="hoverable"><center>{{$infUs->last_name}}</center></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pull-right">
            {{ $infU->appends(Request::capture()->except('page'))->render() }}
        </div>
        <br>
        <div class="table-responsive">
            <h4>
                Usuarios inactivos {{ $infU_in->total() }}
            </h4>
            <table class="table table-hover table-sm">
                <thead>
                    <tr>
                        <th><center>{{ __('EMAIL') }}</center></th>
                        <th><center>{{ __('NAME') }}</center></th>
                        <th><center>{{ __('LAST NAME') }}</center></th>
                    </tr>
                </thead>
                <tbody id="tableCustomer">
                    @foreach ($infU_in as $infUs)
                        <tr>
                            <td class="hoverable"><center>{{$infUs->user->email}}</center></td>
                            <td class="hoverable"><center>{{$infUs->name}}</center></td>
                            <td class="hoverable"><center>{{$infUs->last_name}}</center></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pull-right">
            {{ $infU_in->appends(Request::capture()->except('page'))->render() }}
        </div>
        <br>
        <div class="table-responsive">
            <h4>
                Usuarios en espera {{ $infU_es->total() }}
            </h4>
            <table class="table table-hover table-sm">
                <thead>
                    <tr>
                        <th><center>{{ __('EMAIL') }}</center></th>
                        <th><center>{{ __('NAME') }}</center></th>
                        <th><center>{{ __('LAST NAME') }}</center></th>
                    </tr>
                </thead>
                <tbody id="tableCustomer">
                    @foreach ($infU_es as $infUs)
                        <tr>
                            <td class="hoverable"><center>{{$infUs->user->email}}</center></td>
                            <td class="hoverable"><center>{{$infUs->name}}</center></td>
                            <td class="hoverable"><center>{{$infUs->last_name}}</center></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pull-right">
            {{ $infU_es->appends(Request::capture()->except('page'))->render() }}
        </div>
    </div>
@endsection