@extends('layouts.app')

@section('content')
    <div class="container d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('USERS') }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <button class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#ModalNew">
                <img src="{{ asset('feather/plus-circle.svg') }}">
                {{ __('NEW USER') }}
            </button>
        </div>
    </div>
    <div class="container">
        <form action="{{ url('/User/Search') }}" method="GET">
            {{ csrf_field() }}
            <div class="jumbotron" style="background-color: #fff; border-style: groove; border-color: #E9E9E9; border-width:5px;">
                <div class="panel panel-header">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label>{{ __('EMAIL')}}</label>
                            <input type="text" id="email_user" name="email" class="form-control" value="{{ $email }}" style="text-transform:uppercase;"onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        <div class="col-md-6 -mb-3">
                          <label>{{ __('STATUS*') }}</label>
                          <input type="hidden" id="status_2" value="{{ $statuses }}">
                          <select id="status_id_list" class="form-control" name="statuses" onblur="revisar(this);">
                            <option value="" selected disabled>{{ __('SELECT A STATE') }}</option>
                            @foreach ($status as $statuses)
                                <option value="{{ $statuses->id }}">{{ $statuses->name }}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                </div>
                <div><br></div>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button type="button" class="btn btn-outline-primary" title="LIMPIAR FILTROS DE BUSQUEDA" onclick="Clear();">
                        <img src="{{ asset('feather/trash.svg') }}">
                    </button>
                    <button type="submit" class="btn btn-outline-success">
                        <img src="{{ asset('feather/search.svg') }}">
                        {{ __('SEARCH') }}
                    </button>
                </div>
            </div>
        </form>
            
        <h4>
            THERE ARE {{ $user->total() }}
            @if($user->total() == '1')
                USER
            @else
                USERS
            @endif
            IN TOTAL
        </h4>
        <div class="table-responsive">
            <table class="table table-hover table-sm">
                <thead>
                    <tr>
                        <th><center>{{ __('EMAIL') }}</center></th>
                        <th><center>{{ __('STATUS') }}</center></th>
                        <th><center>{{ __('ACTIONS') }}</center></th>
                    </tr>
                </thead>
                <tbody id="tableCustomer">
                    @foreach ($user as $users)
                        <tr>
                            <td class="hoverable"><center>{{$users->email}}</center></td>
                            <td class="hoverable"><center>{{$users->status->name}}</center></td>
                            <td class="hoverable">
                                <center>
                                    <button value="{{ $users->id }}" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#ModalUpdate" id="UpdateStaus" onclick="Mostrar(this)" title="EDIT">
                                        <img src="{{ asset('feather/edit.svg') }}">
                                    </button>
                                    <a href="/User/{{ $users->id }}/delete" class="btn btn-sm btn-outline-danger" onclick="return confirm('YOU ARE SURE TO DELETE THE USER');" title="DELETE">
                                        <img src="{{ asset('feather/trash-2.svg') }}">
                                    </a>
                                </center>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pull-right">
            {{ $user->appends(Request::capture()->except('page'))->render() }}
        </div>
    </div>
    @include('auth/Modals.new')
    @include('auth/Modals.update')
    <script src="{{ asset('/js/Auth/list.js')}}"></script>
@endsection