<!-- Modal -->
<div class="modal fade" id="ModalNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <!-- Colocamos un input oculto con el token -->
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          <h4 class="modal-title" id="myModalLabel"><center>{{ __('NUEVO TRM') }}</center></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>{{ __('EMAIL*') }}</label>
                    <input type="email" name="EMAIL" class="form-control" id="email" onblur="revisar(this);"/>
                </div>
                <div class="col-md-6 -mb-3">
                  <label>{{ __('STATUS*') }}</label>
                  <select id="status_id" class="form-control" name="STATUS" onblur="revisar(this);">
                    <option value="" selected disabled>{{ __('SELECT A STATE') }}</option>
                    @foreach ($status as $statuses)
                        <option value="{{ $statuses->id }}">{{ $statuses->name }}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-row">
              <div class="col-md-6 mb-3">
                <label>{{ __('PASSWORD*') }}</label>
                <input type="password" id="password" name="PASSWORD" class="form-control" onblur="revisar(this);">
              </div>
              <div class="col-md-6 mb-3">
                <label>{{ __('CONFIRM PASSWORD*') }}</label>
                <input type="password" id="password-confirm" name="CONFIRM PASSWORD" class="form-control" onblur="revisar(this);">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">{{ __('CANCEL') }}</button>
          <button type="button" class="btn btn-outline-success" onclick="newUser();">{{ __('SAVE') }}</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="{{ asset('js/Auth/new.js') }}"></script>