<!-- Modal -->
<div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <!-- Colocamos un input oculto con el token -->
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          <input type="hidden" id="id">
          <h4 class="modal-title" id="myModalLabel"><center>{{ __('EDIT USER') }}</center></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" style="color: #0096DC;">
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>{{ __('EMAIL*') }}</label>
                    <input type="email" name="EMAIL" class="form-control" id="email_edit">
                </div>
                <div class="col-md-6 -mb-3">
                  <label>{{ __('STATUS*') }}</label>
                  <select id="status_id_edit" class="form-control" name="STATUS">
                    <option value="" selected disabled>{{ __('SELECT A STATE') }}</option>
                    @foreach ($status as $statuses)
                        <option value="{{ $statuses->id }}">{{ $statuses->name }}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-row">
              <div class="col-md-6 mb-3">
                <label>{{ __('PASSWORD*') }}</label>
                <input type="password" id="password_edit" name="PASSWORD" class="form-control">
              </div>
              <div class="col-md-6 mb-3">
                <label>{{ __('CONFIRM PASSWORD*') }}</label>
                <input type="password" id="password-confirm_edit" name="CONFIRM PASSWORD" class="form-control">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">{{ __('CANCEL') }}</button>
          <button type="button" class="btn btn-outline-success" onclick="editUser();">{{ __('SAVE') }}</button>
        </div>
      </div>
    </div>
  </div>
  <script src="{{ asset('/js/Auth/update.js')}}"></script>